

from flask import Flask

from flask import request

from flask_cors import CORS

import json

import psycopg2

import json

import pandas as pd

from pandas.io.json import json_normalize

from datetime import datetime

import subprocess

import sys

 

# from bson import json_util

 

app = Flask(__name__)

CORS(app)

 

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# PI_Tracker

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

database_name = "pi_tracker_dev"

 

 

@app.route("/")

def main():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    cur.execute('SELECT * from pi_rally_raw limit 25')

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/access", methods=['GET'])

def feature_request():

    userlan = request.args.get('userid')

 

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = 'select distinct name, lan_id, role from pi_access_ref where "lan_id" ilike ' + "'" + userlan + "'"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/my_access", methods=['GET'])

def my_access_request():

    userlan = request.args.get('userid')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = 'select distinct team from pi_access_ref where "lan_id" ilike ' + "'" + userlan + "'"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/available_pis", methods=['GET'])

def available_pis():

    team = request.args.get('team')

    role = request.args.get('role')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    # This end point will get the first PI that is in Future state to be displayed on EDIT Screen

    query = 'select min(doc_value) as doc_value from doc_control where team ilike ' + "'%" + team + "%'  and doc_type = 'PI' and published='FALSE'"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/available_sprints", methods=['GET'])

def available_sprints():

    team = request.args.get('team')

    role = request.args.get('role')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    # This end point will get all SPRINTS that are active to be displayed on EDIT Screen, there can be only one active sprint at a time

    query = 'select distinct doc_value from doc_control where team ilike ' + "'%" + team + "%'  and doc_type = 'SPRINT' and state='ACTIVE'"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/team_features", methods=['GET'])

def team_features():

    team = request.args.get('team')

    PI = request.args.get('pi')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    # Add STATE filter for != DONE   'epic_kpi_desc','feature_kpi_desc'

    query = 'select feature_id, feature_name, feature_owner, feature_state, feature_release, epic_id, feature_kpi_desc from pi_rally_clean where feature_team = ' + "'" + team + "'" + " AND pi_simplified = '" + PI + "'"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/additional_features", methods=['GET'])

def additional_features():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    # where Feature != DONE & PI != CURR_SELECTED_PI

    query = 'select pi_simplified, array_agg(feature_id), array_agg(feature_name), array_agg(feature_owner), array_agg(feature_kpi_desc) from pi_rally_clean where feature_team = ' + "'" + team + "'" + " AND feature_state != 'Done' group by pi_simplified order by pi_simplified"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/submit_entries', methods=['POST'])

def submit_entries():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    conn.autocommit = True

    data = request.json

    # Delete Prior

    tdoc = data[1]['doc_type']

    tdoc_value = data[1]['doc_value']

    tteam = data[1]['team']

    delete = "DELETE from pi_entry_base where doc_type = '{}' AND doc_value = '{}' AND team = '{}'".format(tdoc,

                                                                                                           tdoc_value,

                                                                                                           tteam)

    cur.execute(delete)

 

    for i in data:

        # Landing table column order

        team = i['team']

        doc = i['doc_type']

        doc_value = i['doc_value']

        feat = i['featid']

        featnm = i['featnm']

        busval = i['busval']

        com = i['commit']

        stat = i['status']

        rmv_rsn = i['rmv_rsn']

        cry = i['carry']

        own = i['owner']

        note = i['notes']

        user = i['user']

        time = i['time']

        query = "INSERT INTO pi_entry_base VALUES ('" + team + "','" + doc + "','" + doc_value + "','" + feat + "','" + featnm + "','" + busval + "','" + com + "','" + stat + "','" + rmv_rsn + "','" + cry + "','" + own + "','" + note + "','" + user + "','" + time + "')"

        cur.execute(query)

 

    # Update Draft IND

    draft_true = "UPDATE doc_control SET draft = 'TRUE' where team = '{}' and doc_type = '{}' and doc_value = '{}'".format(

        tteam, tdoc, tdoc_value)

    cur.execute(draft_true)

    print("All done writing values to database")

 

    cur.close()

 

    return 'Success!'

 

 

@app.route("/update_doc_control", methods=['GET'])

def update_doc_control():

    team = request.args.get('team')

    doc_type = request.args.get('doc_type')

    doc_value = request.args.get('doc_value')

    last_char = doc_value[-1]

    print(last_char)

    print(doc_value)

    print(doc_type)

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    conn.autocommit = True

    cur = conn.cursor()

    # Lock file, add doc_control published ind

    query = "UPDATE doc_control SET editor='ADMIN', state='FINAL', published='TRUE', draft='FALSE' WHERE team='" + team + "' AND doc_type='" + doc_type + "' AND doc_value = '" + doc_value + "'"

    cur.execute(query)

    if doc_type == "PI":

        # Query to activate first sprint when PI is submitted by the admin.

        update_next_sprint = "UPDATE doc_control SET state='ACTIVE' where team='" + team + "' and doc_value = ( select min(doc_value) as doc_value from doc_control where doc_type='SPRINT' and  doc_value like '" + doc_value + "%' )"

        cur.execute(update_next_sprint)

    elif doc_type == "SPRINT" and last_char != "5":

        # Query to activate next sprint when one sprint is submitted

        update_next_sprint = "UPDATE doc_control SET state='ACTIVE' where team='" + team + "' and doc_value = (select min(doc_value) from doc_control where doc_type='SPRINT' and doc_value > '" + doc_value + "' ) "

        cur.execute(update_next_sprint)

 

    # checking if the team has records in aggregrate table

    query = "select count(1) from pi_entry_aggregated where team='" + team + "' AND doc_type='" + doc_type + "' AND doc_value = '" + doc_value + "'"

    cur.execute(query)

 

    tester = cur.fetchone()

    test = pd.DataFrame(data=tester, columns=["value"])

    verify = test["value"].iloc[0]

    print(query)

    print(verify)

 

    # If no records, execute R script to assign Y current rec indicator and the state of document

    if verify != 0:

        query = "UPDATE pi_entry_aggregated SET curr_rcd_ind ='N' WHERE team='" + team + "' AND doc_type='" + doc_type + "' AND doc_value = '" + doc_value + "'"

        cur.execute(query)

 

    # ADD EXECUTION OF RSCRIPT

    cmd = 'Rscript pi_aggregated_cli.R {} {} "{}"'.format(doc_type, doc_value, team)

 

    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    output, errors = p.communicate()

 

    # Adding the Project manager values to the aggregated table

    query = "UPDATE pi_entry_aggregated SET pm_name = access.name from  pi_entry_aggregated base inner join (select distinct a.name , a.team  from pi_access_ref a where role = 'PM' ) access on base.team = access.team '"

    cur.execute(query)

    cur.close()

    return "Success!"

 

 

@app.route("/update_doc_control_draft", methods=['GET'])

def update_doc_control_draft():

    team = request.args.get('team')

    doc_type = request.args.get('doc_type')

    doc_value = request.args.get('doc_value')

    last_char = doc_value[-1]

 

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    conn.autocommit = True

    cur = conn.cursor()

 

    # Assing PO, as they only can save the drafts

    query = "UPDATE doc_control SET editor='PO', draft='TRUE' WHERE team='" + team + "' AND doc_type='" + doc_type + "' AND doc_value = '" + doc_value + "'"

    cur.execute(query)

 

    # checking if the team has records in aggregrate table

    query = "select count(1) from pi_entry_aggregated where team='" + team + "' AND doc_type='" + doc_type + "' AND doc_value = '" + doc_value + "'"

    cur.execute(query)

    tester = cur.fetchone()

    test = pd.DataFrame(data=tester, columns=["value"])

    verify = test["value"].iloc[0]

 

    if verify != 0:

        # Setting existing records as N

        query = "UPDATE pi_entry_aggregated SET curr_rcd_ind='N'  WHERE team='" + team + "' AND doc_type='" + doc_type + "' AND doc_value = '" + doc_value + "'"

        cur.execute(query)

 

    # ADD EXECUTION OF RSCRIPT

    # If no records, execute R script to assign Y current rec indicator and the state of document

    cmd = 'Rscript pi_aggregated_cli_draft.R {} {} "{}"'.format(doc_type, doc_value, team)

 

    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    output, errors = p.communicate()

 

    # Adding the Project manager values to the aggregated table

    query = "select count(1) from pi_entry_aggregated where team='" + team + "' AND doc_type='" + doc_type + "' AND doc_value = '" + doc_value + "'"

    cur.execute(query)

    tester = cur.fetchone()

    test = pd.DataFrame(data=tester, columns=["value"])

    verify = test["value"].iloc[0]

    print(query)

    print(verify, "fourth time, after R script condition")

 

    query = "UPDATE pi_entry_aggregated SET pm_name = access.pm_name from  pi_entry_aggregated base inner join (select distinct a.name as pm_name, b.name as po_name  from pi_access_ref a join  (select distinct name, team from pi_access_ref where role='PO') b on a.team = b.team  where role = 'PM' order by a.name, b.name) access on base.owner = access.po_name where base.team = '" + team + "'"

    cur.execute(query)

    cur.close()

    return "Success!"

 

 

@app.route("/wildcard", methods=['GET'])

def wildcard():

    search = request.args.get('search')

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "SELECT feature_id, feature_name, feature_owner, feature_kpi_desc from pi_rally_clean where feature_team ilike '%" + team + "%' AND (feature_id ilike '%" + search + "%' OR feature_name ilike '%" + search + "%' OR feature_owner ilike '%" + search + "%')"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/view_late_teams", methods=['GET'])

def view_late_teams():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    # Query to identify teams whose submission is not yet in, past the due date.

    query = "SELECT a.doc_type, a.doc_value, a.team, b.name  from doc_control a join pi_access_ref b on a.team=b.team where b.role='PO' and a.published='FALSE' and current_date > cast( a.final_date as DATE) order by a.doc_type, a.doc_value, a.team "

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data, default=str)

    cur.close()

    return data

 

 

@app.route("/lateteams", methods=['GET'])

def lateteams():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "SELECT a.doc_type, a.doc_value, a.team, b.name  from doc_control a join pi_access_ref b on a.team=b.team where b.role='PO' and a.published='FALSE' and current_date > cast( a.final_date as DATE) order by  a.team,  a.doc_value"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data, default=str)

    cur.close()

    return data

 

 

@app.route("/submission_exists", methods=['GET'])

def submission_exists():

    doctype = request.args.get('doc_type')

    docval = request.args.get('doc_value')

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "SELECT featid, featnm, busval, commit, status, carry, owner, notes, rmv_rsn from pi_entry_base where team ilike '%{}%' AND doc_value = '{}' AND doc_type = '{}'".format(

        team, docval, doctype)

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route("/get_po", methods=['GET'])

def get_po():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "SELECT distinct name from pi_access_ref where role = 'PO' and team = '{}' ".format(team)

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/view', methods=["GET"])

def view():

    team = request.args.get('team')

    pi = request.args.get('pi')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select doc_type, doc_value, epic_id, epic_name, array_agg(post_planning), array_agg(featid), array_agg(featnm), array_agg(busval), array_agg(commit), array_agg(status), array_agg(rmv_rsn), array_agg(carry), array_agg(carry_count), array_agg(pi_id), array_agg(notes), team from pi_entry_aggregated where ( team='{}' or pm_name='{}' ) and doc_value = '{}'  and curr_rcd_ind = 'Y' group by team, epic_id, epic_name, doc_type, doc_value order by team, doc_value, epic_id ".format(

        team, team, pi)

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/admin_docs', methods=["GET"])

def admin_docs():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = " select distinct doc_type, doc_value, active_date, final_date from doc_control where published ='FALSE' order by doc_value "

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data, default=str)

    cur.close()

    return data

 

 

@app.route('/admin_update_doc', methods=["GET"])

def admin_update_doc():

    active_date = request.args.get('active_date')

    final_date = request.args.get('final_date')

    doc_type = request.args.get('doc_type')

    doc_value = request.args.get('doc_value')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    conn.autocommit = True

    query = "UPDATE doc_control SET active_date='{}', final_date='{}' WHERE doc_type='{}' AND doc_value = '{}'".format(

        active_date, final_date, doc_type, doc_value)

    cur.execute(query)

    cur.close()

    return "Success!"

 

 

@app.route('/admin_create_doc', methods=["GET"])

def admin_create_doc():

    doc_start = request.args.get('doc_start')

    doc_end = request.args.get('doc_end')

    doc_type = request.args.get('doc_type')

    doc_value = request.args.get('doc_value')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    conn.autocommit = True

    q_one = "SELECT count(*) from doc_control where doc_type='{}' and doc_value='{}'".format(doc_type, doc_value)

    cur.execute(q_one)

    tester = cur.fetchone()

    test = pd.DataFrame(data=tester, columns=["value"])

    verify = test["value"].iloc[0]

    # print(tester)

    if verify == 0:

        data = "NEW"

        q_two = "SELECT distinct team from team_management_ref"

        cur.execute(q_two)

        teams = cur.fetchall()

        # teams = json.dumps(teams)

        df = pd.DataFrame(data=teams, columns=["team"])

        for i in df["team"]:

            q = "INSERT INTO doc_control VALUES ('{}','{}','{}','{}','{}','FUTURE','LOCK', 'FALSE', 'FALSE')".format(

                doc_type, doc_value, i, doc_start, doc_end)

            cur.execute(q)

            conn.commit()

    else:

        data = "BAD"

 

    return data

 

 

@app.route('/admin_rmv_doc', methods=["GET"])

def admin_rmv_doc():

    doc_type = request.args.get('doc_type')

    doc_value = request.args.get('doc_value')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    conn.autocommit = True

    q_one = "SELECT count(*) from doc_control where doc_type='{}' and doc_value='{}'".format(doc_type, doc_value)

    cur.execute(q_one)

    tester = cur.fetchone()

    test = pd.DataFrame(data=tester, columns=["value"])

    verify = test["value"].iloc[0]

    # print(tester)

    if verify == 0:

        data = "BAD"

    else:

        data = "GOOD"

        q = "DELETE FROM doc_control WHERE doc_type='{}' and doc_value='{}'".format(doc_type, doc_value)

        cur.execute(q)

    return data

 

 

@app.route('/admin_team_values', methods=["GET"])

def admin_team_values():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select lan_id, name, role from pi_access_ref where team = '{}'".format(team)

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/admin_team_schedule', methods=["GET"])

def admin_team_schedule():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    # Query to obtain the schedule of a team.

    query = "select doc_type, doc_value, team, active_date, final_date from doc_control where team = '{}' order by team, doc_value".format(

        team)

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/admin_update_users', methods=['POST'])

def admin_update_users():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    conn.autocommit = True

    data = request.json

    team = request.args.get('team')

    query = "select distinct lan_id from pi_access_ref where role = 'ADMIN' and  team = '{}'".format(team)

    df = pd.DataFrame()

    df = pd.read_sql(query, conn)

    new_lan_list = []

    for i in data:

        lan_id = i['lan_id']

        new_lan_list.append(lan_id)

    df_lan_id = df['lan_id'].tolist()

    result = [x for x in df_lan_id if x not in new_lan_list]

    for r in result:

        delete_lan_id = "DELETE from pi_access_ref where lan_id = '{}'".format(r)

        cur.execute(delete_lan_id)

 

    delete = "DELETE from pi_access_ref where team = '{}'".format(team)

    cur.execute(delete)

 

    for i in data:

        # Landing table column order

        lan_id = i['lan_id']

        name = i['name']

        team = i['team']

        role = i['role']

        query = "DELETE from pi_access_ref where lan_id = '{}'".format(lan_id)

        cur.execute(query)

 

        if role == 'ADMIN':

            query = "INSERT INTO pi_access_ref select '{}','{}',  team, '{}', 'Y' FROM (SELECT distinct team FROM team_management_ref) x ".format(

                lan_id, name, role)

            cur.execute(query)

        else:

            query = "INSERT INTO pi_access_ref VALUES ('{}','{}','{}','{}','Y')".format(lan_id, name, team, role)

            cur.execute(query)

 

    print("All done writing values to database")

 

    cur.close()

 

    return 'Success!'

 

 

@app.route('/admin_run_update', methods=['GET'])

def admin_run_update():

    cmd = 'Rscript doc_control_update.R'

    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    output, errors = p.communicate()

    return 'Success!'

 

 

@app.route('/view_available_team', methods=["GET"])

def view_available_team():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select distinct team from pi_entry_aggregated where curr_rcd_ind = 'Y' "

    # query = "select name from (select * from ( select distinct name, 0 as prior from pi_access_ref where role = \'PM\' order by name ) abc union select distinct team, 1 as prior from pi_access_ref order by prior) xyz "

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/view_available_pi', methods=["GET"])

def view_available_pi():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    # Getting all sprints and Pis which are submitted

    query = "select distinct doc_value from pi_entry_aggregated where team = '{}' and curr_rcd_ind='Y'  ".format(team)

    # query = "select distinct pi_id from ( select y.pm_name, x.* from pi_entry_aggregated x join (select distinct a.name as pm_name, b.name as po_name  from pi_access_ref a join  (select distinct name, team from pi_access_ref where role='PO') b on a.team = b.team  where role = 'PM' order by a.name, b.name ) y on x.owner = y.po_name ) xyz where team = '{}' ".format(team)

 

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/view_available_pi_for_pms', methods=["GET"])

def view_available_pi_for_pms():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select distinct doc_value from pi_entry_aggregated where pm_name = '{}' and curr_rcd_ind = 'Y' ".format(

        team)

    # query = "select distinct pi_id from ( select y.pm_name, x.* from pi_entry_aggregated x join (select distinct a.name as pm_name, b.name as po_name  from pi_access_ref a join  (select distinct name, team from pi_access_ref where role='PO') b on a.team = b.team  where role = 'PM' order by a.name, b.name ) y on x.owner = y.po_name ) xyz where pm_name = '{}' ".format(team)

 

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/view_updated_date', methods=["GET"])

def view_updated_date():

    team = request.args.get('team')

    pi_id = request.args.get('pi_id')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select distinct doc_type, doc_value from pi_entry_aggregated where team = '{}' and pi_id = '{}' and curr_rcd_ind = 'Y' ".format(

        team, pi_id)

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data, default=str)

    cur.close()

    return data

 

 

@app.route('/view_owners', methods=["GET"])

def view_owners():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select distinct name, role from pi_access_ref where team = '{}' and (role='PO' or role='PM')".format(team)

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/preview_pms', methods=["GET"])

def preview_pms():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select distinct name, array_agg(team) from pi_access_ref where role='PM' group by name"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/preview_teams', methods=["GET"])

def preview_teams():

    team = request.args.get('team')

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select distinct team from pi_access_ref"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/team_management', methods=["GET"])

def team_management():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    query = "select distinct team from team_management_ref"

    cur.execute(query)

    data = cur.fetchall()

    data = json.dumps(data)

    cur.close()

    return data

 

 

@app.route('/team_management_update', methods=['POST'])

def team_management_update():

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database=database_name, user="axservice",

                            password="service")

    cur = conn.cursor()

    conn.autocommit = True

    data = request.json

    team = request.args.get('team')

    delete = "DELETE from team_management_ref where team is not null"

    cur.execute(delete)

 

    for i in data:

        query = "INSERT INTO team_management_ref VALUES ('{}')".format(i)

        cur.execute(query)

    # New Team will be added to the doc_control directly they will have active sprint and future documents added to the schedule

    query_doc_control = "INSERT INTO doc_control select doc_type, doc_value, '{}' , active_date, final_date, 'FUTURE', 'ADMIN',  'FALSE', 'FALSE' from doc_control a where team = ( select min(team) from doc_control ) and ( (current_date < cast ( final_date as date )  and doc_type ='SPRINT' ) or (current_date < cast ( active_date as date )  and doc_type ='PI' ) )    order by doc_value".format(

        i)

    cur.execute(query_doc_control)

    print("All done writing values to database")

 

    cur.close()

 

    return 'Success!'

 

 

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#

# Heatmap

#

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 

 

@app.route("/plus_one_email", methods=['POST'])

def plus_one_email():

    epic_id = request.args.get('epic_id')

    userlan = request.args.get('userid')

    user_vert = request.args.get('bv_name')

    user_vert_info = request.args.get('bv_info')

    user_comm = request.args.get('cmt_info')

 

    # Actual CLI input

    cmd = """echo -e "This is an automated email message notifying you that user {userlan} sent a plus one request on {epic_id}.\n

    Business Vertical: {user_vert}\n

    Alignment Description: {user_vert_info}\n

    Comment Information: {user_comm}\n

    This information will reflect in the Heatmap by 7AM EST time."  |

    mailx -r 'GD&A_Heatmap@Cigna.com' -S smtp=testapp.silver.com:25 -s "Attention: Plus One Request" -v 'M&R_Intake@Cigna.com'""".format(

        epic_id=str(epic_id), userlan=str(userlan), user_vert=str(user_vert), user_vert_info=str(user_vert_info),

        user_comm=str(user_comm))

    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    output, errors = p.communicate()

    return 'message sent!'

 

 

@app.route('/checkuser', methods=['GET'])

def check():

    # here we want to get the value of user (i.e. ?user=some-value)

    # /query?data={"id":"1","feature":"2"}

    user = request.args.get('user')

    print(user)

    qstring = 'select * from "DFDB_EDIT" where "lanid" = ' + "'" + user + "'"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchone()

    cur.close()

    if results == None:

        return "It looks like we haven't added you to our personal directory!"

    else:

        results = json.dumps(results)

        return results

    # data = json.loads(data)

    # return 'Success'

 

 

@app.route('/checkuserpublic', methods=['GET'])

def checkuserpublic():

    # here we want to get the value of user (i.e. ?user=some-value)

    # /query?data={"id":"1","feature":"2"}

    user = request.args.get('user')

    print(user)

    qstring = 'select * from "DFDB" where "lanid" = ' + "'" + user + "'"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchone()

    cur.close()

    if results == None:

        return "It looks like we haven't added you to our personal directory!"

    else:

        results = json.dumps(results)

        return results

    # data = json.loads(data)

    # return 'Success'

 

    #

    #

    #   THIS SECTION IS FOR EPIC FEATURE QUESTION PROJECT

    #

    #

 

 

@app.route('/checkfeatures', methods=['GET'])

def checkfeatures():

    user = request.args.get('user')

    print(user)

    qstring = 'select "feature_id", "feature_name", "feature_state", "epic_id", "epic_name", "epic_state", "feature_owner", "hm_ind" from hm_cert_check_features '

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "NORESULTS"

    else:

        results = json.dumps(results)

        return results

 

 

# Changing to only point to Certified Features

@app.route('/checkfeatures_query', methods=['GET'])

def checkfeatures_query():

    query = request.args.get('query')

    qstring = 'select distinct "feature_id", "feature_name", "feature_state", "epic_id", "epic_name", "epic_state", "feature_owner", "hm_ind" from hm_cert_check_features where "feature_name" ilike ' + "'%" + query + "%'" + ' OR "feature_id" ilike ' + "'%" + query + "%'" + ' OR "feature_owner" ilike ' + "'%" + query + "%'"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "NORESULTS"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/checkfeatures_state', methods=['GET'])

def checkfeatures_state():

    query = request.args.get('query')

    qstring = 'select distinct "feature_id", "feature_name", "feature_state", "epic_id", "epic_name", "epic_state", "feature_owner", "hm_ind" from hm_cert_check_features where "feature_state" ' + query

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "NORESULTS"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/checkepicsplusone', methods=['GET'])

def checkepicsplusone():

    qstring = 'select distinct "Parent.FormattedID", "Parent.Name", "Parent.State._refObjectName", "hm_ind" from hm_epic_feature_reference where "hm_ind" = ' + "'Y'"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "It looks like you dont have any Epics assigned!"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/checkepicsplusone_query', methods=['GET'])

def checkepicsplusone_query():

    query = request.args.get('query')

    qstring = 'select distinct "Parent.FormattedID", "Parent.Name", "Parent.State._refObjectName", "hm_ind" from hm_epic_feature_reference where ("hm_ind" = ' + "'Y'" + ') AND ( "Parent.Name" ilike ' + "'%" + query + "%'" + ' OR "Parent.FormattedID" like ' + "'%" + query + "%'" + ')'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "NORESULTS"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/checkepicsplusone_state', methods=['GET'])

def checkepicsplusone_state():

    query = request.args.get('query')

    qstring = 'select distinct "Parent.FormattedID", "Parent.Name", "Parent.State._refObjectName", "hm_ind" from hm_epic_feature_reference where ("hm_ind" = ' + "'Y'" + ') AND "Parent.State._refObjectName" ' + query

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

   cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "NORESULTS"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/plusonetagref', methods=['GET'])

def plusonetagref():

    # here we want to get the value of user (i.e. ?user=some-value)

    # /query?data={"id":"1","feature":"2"}

    qstring = 'select distinct(tag_cleaned) from hm_tag_ref'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    if results == None:

        return "No tags are here for some reason!"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/checkepics', methods=['GET'])

def checkepics():

    qstring = 'select distinct "Parent.FormattedID", "Parent.Name", "Parent.State._refObjectName", "hm_ind" from hm_epic_feature_reference '

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "It looks like you dont have any Epics assigned!"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/checkepics_query', methods=['GET'])

def checkepics_query():

    query = request.args.get('query')

    qstring = 'select distinct "Parent.FormattedID", "Parent.Name", "Parent.State._refObjectName", "hm_ind" from hm_epic_feature_reference where "Parent.Name" ilike ' + "'%" + query + "%'" + ' OR "Parent.FormattedID" like ' + "'%" + query + "%'"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "NORESULTS"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/checkepics_state', methods=['GET'])

def checkepics_state():

    query = request.args.get('query')

    qstring = 'select distinct "Parent.FormattedID", "Parent.Name", "Parent.State._refObjectName", "hm_ind" from hm_epic_feature_reference where "Parent.State._refObjectName" ' + query

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    print(results)

    cur.close()

    if results == None:

        return "NORESULTS"

    else:

        results = json.dumps(results)

        return results

 

 

@app.route('/feature_qs', methods=['GET'])

def feature_questions():

    qstring = 'select distinct "hm_feature_questions_reference"."Feature_Question_ID", "hm_feature_questions_reference"."Feature_Question_Text", ARRAY_AGG("hm_feature_questions_reference"."Feature_Possible_Answer") from hm_feature_questions_reference group by  "hm_feature_questions_reference"."Feature_Question_ID", "hm_feature_questions_reference"."Feature_Question_Text" order by "hm_feature_questions_reference"."Feature_Question_ID"'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    return json.dumps(results)

 

 

@app.route('/epic_qs', methods=['GET'])

def epic_questions():

    qstring = 'select distinct "hm_epic_questions_reference"."Quest_ID", "hm_epic_questions_reference"."Quest_Text", ARRAY_AGG("hm_epic_questions_reference"."Quest_Ans") from hm_epic_questions_reference group by  "hm_epic_questions_reference"."Quest_ID", "hm_epic_questions_reference"."Quest_Text" order by  "hm_epic_questions_reference"."Quest_ID"'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    return json.dumps(results)

 

 

@app.route('/datadomains', methods=['GET'])

def datadomains():

    qstring = 'select "Primary", ARRAY_AGG("hm_datadomains_ref"."Sub") from hm_datadomains_ref group by  "hm_datadomains_ref"."Primary" order by  "hm_datadomains_ref"."Primary"'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    return json.dumps(results)

 

 

@app.route('/datadomains_query', methods=['GET'])

def datadomains_query():

    # here we want to get the value of user (i.e. ?user=some-value)

    # /query?data={"id":"1","feature":"2"}

    query = request.args.get('query')

    qstring = 'select "Primary", ARRAY_AGG("hm_datadomains_ref"."Sub") from hm_datadomains_ref where "Primary" = ' + "'" + query + "'" + ' group by  "hm_datadomains_ref"."Primary" order by  "hm_datadomains_ref"."Primary"'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    return json.dumps(results)

 

 

@app.route('/feature_write', methods=['POST'])

def feature_write():

    # here we want to get the value of user (i.e. ?user=some-value)

    # /query?data={"id":"1","feature":"2"}

 

    # Variables to pull through

    quest_id = request.args.get('quest_id')

    quest_ans = request.args.get('quest_ans')

    feature_id = request.args.get('feature_id')

    epic_id = request.args.get('epic_id')

    prim_data_domain = request.args.get('prim_data_domain')

    sec_data_domain = request.args.get('sec_data_domain')

    user = request.args.get('user')

 

    qstring = r"INSERT into hm_testwrite_features VALUES ('" + epic_id + "', '" + feature_id + "', '" + quest_id + "', '" + quest_ans + "', '" + prim_data_domain + "', '" + sec_data_domain + "', '" + user + "', current_timestamp )"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    num_effect = cur.rowcount

    qstring = r"UPDATE hm_cert_check_features SET hm_ind = 'Y' WHERE feature_id = '" + feature_id + "'"

    cur.execute(qstring)

    cur.close()

    if num_effect == 0:

        return "FAILURE"

    else:

        return "SUCCESS"

 

 

@app.route('/epic_write', methods=['POST'])

def epic_write():

    # here we want to get the value of user (i.e. ?user=some-value)

    # /query?data={"id":"1","feature":"2"}

 

    # Variables to pull through

    quest_id = request.args.get('quest_id')

    quest_ans = request.args.get('quest_ans')

    epic_id = request.args.get('epic_id')

    category = request.args.get('category')

    user = request.args.get('user')

 

    qstring = r"INSERT into hm_testwrite_epics VALUES ('" + epic_id + "', '" + quest_id + "', '" + quest_ans + "', '" + category + "', '" + user + "', current_timestamp )"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    num_effect = cur.rowcount

    qstring = r"UPDATE hm_epic_feature_reference SET hm_ind = 'Y' WHERE " + '"Parent.FormattedID"' + " = '" + epic_id + "'"

    cur.execute(qstring)

    cur.close()

    if num_effect == 0:

        return "FAILURE"

    else:

        return "SUCCESS"

 

 

@app.route('/feature_write_check', methods=['GET'])

def feature_write_check():

    # Variables to pull through

    feature_id = request.args.get('feature_id')

    qstring = 'SELECT DISTINCT ON (A."Quest_ID") A."Feat_ID", A."Quest_ID", A."Quest_Ans", A."Prim_Data_Domain", A."Sec_Data_Domain", A."User_Added", A."Date_Added", B."Feature_Question_Text" from hm_testwrite_features as A inner join hm_feature_questions_reference as B on A."Quest_ID" = B."Feature_Question_ID" where A."Feat_ID" = ' + "'" + feature_id + "' order by " + 'A."Quest_ID", A."Date_Added" DESC'

    # qstring = 'SELECT DISTINCT ON ("Quest_ID") "Feat_ID", "Quest_ID", "Quest_Ans", "Prim_Data_Domain", "Sec_Data_Domain", "User_Added", "Date_Added" from hm_testwrite_features where "Feat_ID" = '+"'"+feature_id+"' order by "+'"Quest_ID", "Date_Added" DESC'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    return json.dumps(results, default=str)

 

 

@app.route('/epic_write_check', methods=['GET'])

def epic_write_check():

    # Variables to pull through

    epic_id = request.args.get('epic_id')

    qstring = 'SELECT DISTINCT ON (A."Quest_ID") A."Epic_ID", A."Quest_ID", A."Quest_Ans", A."User_Added", A."Date_Added", B."Quest_Text" from hm_testwrite_epics as A inner join hm_epic_questions_reference as B on A."Quest_ID" = B."Quest_ID" where A."Epic_ID" = ' + "'" + epic_id + "' order by " + 'A."Quest_ID", A."Date_Added" DESC'

    # qstring = 'SELECT DISTINCT ON ("Quest_ID") "Feat_ID", "Quest_ID", "Quest_Ans", "Prim_Data_Domain", "Sec_Data_Domain", "User_Added", "Date_Added" from hm_testwrite_features where "Feat_ID" = '+"'"+feature_id+"' order by "+'"Quest_ID", "Date_Added" DESC'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    return json.dumps(results, default=str)

 

 

@app.route('/me_too_questions', methods=['GET'])

def me_too_questions():

    # Variables to pull through

    qstring = 'select distinct "hm_plus_questions_reference"."Quest_ID", "hm_plus_questions_reference"."Quest_Text", ARRAY_AGG("hm_plus_questions_reference"."Quest_Ans") from hm_plus_questions_reference group by  "hm_plus_questions_reference"."Quest_ID", "hm_plus_questions_reference"."Quest_Text" order by  "hm_plus_questions_reference"."Quest_ID"'

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    results = cur.fetchall()

    cur.close()

    return json.dumps(results)

 

 

@app.route('/plus_one_write', methods=['POST'])

def plus_one_write():

    # here we want to get the value of user (i.e. ?user=some-value)

    # /query?data={"id":"1","feature":"2"}

 

    # Variables to pull through

    quest_id = request.args.get('quest_id')

    quest_ans = request.args.get('quest_ans')

    epic_id = request.args.get('epic_id')

    category = request.args.get('category')

    user = request.args.get('user')

    tag = request.args.get('tag')

    bv_align = request.args.get('bv_align')

    cmt_align = request.args.get('cmt_align')

 

    qstring = r"INSERT into hm_testwrite_plus_one VALUES ('" + epic_id + "', '" + quest_id + "', '" + quest_ans + "', '" + category + "', '" + user + "', '" + tag + "', '" + bv_align + "', '" + cmt_align + "', current_timestamp )"

    conn = psycopg2.connect(host="cilsdbxd59019.silver.com", database="roadmap", user="axservice", password="service")

    conn.autocommit = True

    cur = conn.cursor()

    cur.execute(qstring)

    num_effect = cur.rowcount

    cur.close()

    if num_effect == 0:

        return "FAILURE"

    else:

        return "SUCCESS"

 

 

if __name__ == '__main__':

    app.run(host='0.0.0.0', port=9999)
